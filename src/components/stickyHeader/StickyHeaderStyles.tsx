import {styled, alpha} from "@mui/material";
import {Box, AppBar, Toolbar, InputBase} from '@mui/material';
import FilterAltOutlinedIcon from '@mui/icons-material/FilterAltOutlined';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';

export const MainContainer = styled(Box)(({theme}) => ({
  position: 'fixed',
  width: '100%',
  top: '0',
  zIndex: '1'
}));

export const Container = styled(AppBar)(({theme}) => ({
  color: 'inherit'
}));

export const Bar = styled(Toolbar)(({theme}) => ({
  color: 'inherit'
}));

export const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  color: 'white',
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(3),
    width: 'auto',
  },
}));

export const InputContainer = styled(InputBase)(({theme}) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(1)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
}));

export const FilterIcon = styled(FilterAltOutlinedIcon)(({theme}) => ({
  color: 'white'
}));

export const ShoppingIcon = styled(ShoppingCartOutlinedIcon)(({theme}) => ({
  color: 'white'
}));
