import React from 'react';
import {MainContainer, Container, Bar, Search, InputContainer, FilterIcon, ShoppingIcon} from "./StickyHeaderStyles";
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import Badge from '@mui/material/Badge';
import {response} from "../../services/interfaces";

interface StickyHeaderProps {
  data: response[];
  filteredData: response[];
  searchedData: response[];
  showFilter: boolean;
  setShowFilter: React.Dispatch<React.SetStateAction<boolean>>;
  setSearchedData: React.Dispatch<React.SetStateAction<response[]>>;
  setShowSearched: React.Dispatch<React.SetStateAction<boolean>>;
}

function StickyHeader({data, filteredData, searchedData, showFilter, setShowFilter, setSearchedData, setShowSearched}: StickyHeaderProps) {
  const handleIcon = () => {
    if (showFilter == false) {
      setShowFilter(true);
    } else {
      setShowFilter(false);
    }
  }

  const handleSearchBar = (e: { currentTarget: { value: string; }; }) => {
    const value = e.currentTarget.value.toLowerCase();
    console.log(value);
    if (value !== undefined && value !== '') {
      setShowFilter(false);
      setShowSearched(true);
      const newFilteredObject = data.filter(response => response.title.toLowerCase().includes(value));
      setSearchedData(newFilteredObject);
    } else {
      setShowFilter(false);
      setShowSearched(false);
    }
  }

  return (
    <MainContainer sx={{ flexGrow: 1 }}>
      <Container position="static">
        <Bar>
          <Search>
            <InputContainer placeholder="Search..." onChange={handleSearchBar}/>
          </Search>
          <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
            <IconButton size="large" aria-label="show 4 new mails" color="inherit">
              <FilterIcon />
            </IconButton>
          </Box>
          <Box sx={{ flexGrow: 1 }} />
          <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
            <IconButton size="large" aria-label="show 4 new mails" color="inherit">
              <Badge badgeContent={filteredData.length} color="error" onClick={handleIcon}>
                <ShoppingIcon />
              </Badge>
            </IconButton>
          </Box>
        </Bar>
      </Container>
    </MainContainer>
  );
}

export default StickyHeader;
