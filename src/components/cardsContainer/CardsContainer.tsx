import React, {useEffect, useState} from 'react';
import {Container} from './CardsContainerStyles';
import {Grid} from "@mui/material";
import Card from "../card/Card";
import {response} from "../../services/interfaces";
import DateFormat, {SortByDate} from "../../services/dateFormatter";

interface CardsProps {
  data: response[];
  filteredData: response[];
  searchedData: response[];
  showFilter: boolean;
  setFilteredData: React.Dispatch<React.SetStateAction<response[]>>;
  showSearched: boolean;
  setViewDate: React.Dispatch<React.SetStateAction<string>>;
}

function CardContainer({data, filteredData, searchedData, showFilter, setFilteredData, showSearched, setViewDate}: CardsProps) {

  let object: any[];
  if (showFilter && filteredData.length > 0) {
    object = filteredData;
  } else if (showSearched && searchedData.length > 0) {
    object = searchedData;
  } else {
    object = data;
  }

  SortByDate(object);

  window.addEventListener('scroll', function (e) {
    const view = window.pageYOffset / 640;
    const num = Math.round(~~view) * 3;
    setViewDate(DateFormat(object[num].date));
  });

  return (
    <Container sx={{ flexGrow: 1 }} container spacing={2}>
      {object.map(card => {
        const date = DateFormat(card.date);
        const startTime = DateFormat(card.startTime);
        const endTime = DateFormat(card.endTime);
        return (
          <Grid item xs={4}>
            <Card data={data} filteredData={filteredData} id={card._id} title={card.title} date={date} img={card.flyerFront} city={card.city} country={card.country} start={startTime} end={endTime} setFilteredData={setFilteredData}/>
          </Grid>
        );
      })}
    </Container>
  );
}

export default CardContainer;
