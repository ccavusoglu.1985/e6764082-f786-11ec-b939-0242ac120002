import {styled, Grid, Typography} from "@mui/material";

export const MainContainer = styled(Grid)(({theme}) => ({
  position: 'fixed',
  width: '100%',
  top: '80px',
  backgroundColor: '#cee6f3',
  opacity: '0.5',
  zIndex: '1'
}));

export const Title = styled(Typography)(({theme}) => ({
  textAlign: 'center',
  color: '#477fe8',
  fontWeight: '900'
}));
