import React from 'react';
import {MainContainer, Title} from "./StickyDateStyles";
import {Grid} from "@mui/material";

interface StickyDateProps {
  date: string;
}

function StickyDate({date} : StickyDateProps) {
  return (
    <MainContainer sx={{ flexGrow: 1 }} container spacing={2}>
      <Grid item xs={4}>
        <Title variant="subtitle1" paragraph>
          {date}
        </Title>
      </Grid>
    </MainContainer>
  );
}

export default StickyDate;
