import { styled } from '@mui/material/styles';
import {Card, CardHeader, Avatar, CardMedia, CardActions, IconButton, Typography, Box} from '@mui/material';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import AddCircleIcon from '@mui/icons-material/AddCircle';

export const CardContainer = styled(Card)(({theme}) => ({
  minHeight: '600px'
}));

export const Header = styled(CardHeader)(({theme}) => ({
  minHeight: '80px'
}));

export const Icon = styled(Avatar)(({theme}) => ({

}));

export const Media = styled(CardMedia)(({theme}) => ({

}));

export const Actions = styled(CardActions)(({theme}) => ({
  minHeight: '50px'
}));

export const IconContainer = styled(IconButton)(({theme}) => ({
  position: 'relative',
  float: 'right',
  minHeight: '50px'
}));

export const LocationIcon = styled(LocationOnIcon)(({theme}) => ({
  color: '#6495ED'
}));

export const TextContent = styled(Typography)(({theme}) => ({
  marginBottom: '10px'
}));

export const ContentContainer = styled(Box)(({theme}) => ({
  minHeight: '50px',
  padding: '20px'
}));

export const AddIcon = styled(AddCircleIcon)(({theme}) => ({
  color: '#6495ED'
}));

