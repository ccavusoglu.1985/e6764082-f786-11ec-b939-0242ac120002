import React, {useEffect, useState} from 'react';
import {CardContainer, Header, Icon, Media, Actions, IconContainer, LocationIcon, TextContent, ContentContainer, AddIcon} from './CardStyles';
import {Typography, Box, IconButton} from "@mui/material";
import {response} from "../../services/interfaces";

interface CardProps {
  data: response[];
  filteredData: response[];
  id: string;
  title: string | any;
  date: string;
  img: string;
  city: string;
  country: string;
  start: string;
  end: string;
  setFilteredData: React.Dispatch<React.SetStateAction<response[]>>;
}

function Card({data, filteredData, id, title, date, img, city, country, start, end, setFilteredData} : CardProps) {

  const handleFilteredData = (event: { currentTarget: any; }) => {
    const id = event.currentTarget.id;
    const filteredObject = data.find(data => data._id === id);
    if (filteredObject) {
      const newFilteredObject = filteredData.concat(filteredObject);
      setFilteredData(newFilteredObject);
    }
  };

  const handleGoogleData = (event: { currentTarget: any; }) => {
    const id = event.currentTarget.id;
    const filteredObject = data.find(data => data._id === id);
    if (filteredObject) {
      const city = filteredObject.city;
      window.open("https://maps.google.com?q="+city, '_blank');
    }
  };

  return (
    <CardContainer sx={{ maxWidth: 345 }}>
      <Header
        avatar={
          <Icon aria-label="recipe">
            CC
          </Icon>
        }
        title={title}
        subheader={date}
      />
      <Box
        component="img"
        sx={{
          height: 300,
          width: 350,
        }}
        src={img}
      />
      <Actions disableSpacing>
        <IconButton aria-label="add to favorites">
          <LocationIcon id={id} onClick={handleGoogleData}/>
        </IconButton>
        <Typography variant="body2" color="text.secondary">
          {city}, {country}
        </Typography>
      </Actions>
      <ContentContainer sx={{ flexGrow: 1 }}>
        <TextContent variant="body1" color="text.secondary">
          {start}
        </TextContent>
        <TextContent variant="body1" color="text.secondary">
          {end}
        </TextContent>
      </ContentContainer>
      <IconContainer aria-label="add to favorites">
        <AddIcon onClick={handleFilteredData} id={id}/>
      </IconContainer>
    </CardContainer>
  );
}

export default Card;
