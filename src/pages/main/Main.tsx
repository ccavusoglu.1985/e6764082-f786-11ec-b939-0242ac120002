import React, {useEffect, useState} from 'react';
import './Main.css';
import StickyDate from "../../components/stickyDate/StickyDate";
import StickyHeader from "../../components/stickyHeader/StickyHeader";
import CardContainer from "../../components/cardsContainer/CardsContainer";
import ApiCall from "../../services/apiCall";
import {response} from "../../services/interfaces";

function Main() {
  const [data, setData] = useState<response[]>([]);
  const [filteredData, setFilteredData] = useState<response[]>([]);
  const [searchedData, setSearchedData] = useState<response[]>([]);
  const [showFilter, setShowFilter] = useState(false);
  const [showSearched, setShowSearched] = useState(false);
  const [viewDate, setViewDate] = useState('');

  useEffect(() => {
    ApiCall().then(response => setData(response));
  }, []);

  return (
    <div>
      <StickyHeader data={data} filteredData={filteredData} searchedData={searchedData} showFilter={showFilter} setShowFilter={setShowFilter} setSearchedData={setSearchedData} setShowSearched={setShowSearched}/>
      <StickyDate date={viewDate}/>
      <CardContainer data={data} filteredData={filteredData} searchedData={searchedData} showFilter={showFilter} setFilteredData={setFilteredData} showSearched={showSearched} setViewDate={setViewDate}/>
    </div>
  );
}

export default Main;
