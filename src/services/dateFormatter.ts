import moment from 'moment/moment';
import {response} from './interfaces';

export const DateFormat = (date: string) => {
  const newDate = new Date(date).toString();
  const dateString = moment(newDate).format('MMMM Do YYYY, h:mm:ss a');
  return dateString.toString();
}

export const SortByDate = (object: response[]) => {
  // @ts-ignore
  return object.sort((a, b) => Date.parse(new Date(a.date.split("/").reverse().join("-"))) - Date.parse(new Date(b.date.split("/").reverse().join("-"))))
}

export default DateFormat;
