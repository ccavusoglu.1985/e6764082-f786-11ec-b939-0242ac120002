import {response} from "./interfaces";

async function ApiCall() {
  const data = await fetch('https://tlv-events-app.herokuapp.com/events/uk/london')
      .then<response[]>(res => res.json())
      .catch(e => console.log(e));
  if(!data){
    throw new Error("Api not available")
  }
  return data;
}

export default ApiCall;
