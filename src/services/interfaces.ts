export interface response {
  _id: string;
  title: string;
  date: string;
  flyerFront: string;
  city: string;
  country: string;
  startTime: string;
  endTime: string;
}
