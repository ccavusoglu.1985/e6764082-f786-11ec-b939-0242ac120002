# WebStack des Projektes

Das Projekt wurde mit dem Framework/ mit der Bibliothek React durchgesetzt. Zum Stylen wurde Material UI und Styled Components benutzt.

## Ausführen des Projektes

Um das Projekt auszuführen, muss im Terminal, im Source, npm start ausgeführt werden, oder yarn start.

### Was wird benötitgt?

Um das Projekt lokal testen zu können, wird npm oder yarn benötigt. Die Pakete werden mit npm install oder yarn install installiert.
